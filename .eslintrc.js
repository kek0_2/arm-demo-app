module.exports = {
  parser: "@typescript-eslint/parser",
  extends: "react-app",
  settings: {
    "import/resolver": {
      node: {
        paths: ["src"],
      },
    },
  },
  plugins: ["@typescript-eslint", "react-hooks", "import"],
  rules: {
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": [
      2,
      {
        args: "none",
        ignoreRestSiblings: true,
      },
    ],
    "import/order": [
      "error",
      {
        groups: [
          "internal",
          "builtin",
          "external",
          "parent",
          "sibling",
          "index",
        ],
      },
    ],
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",
    "jsx-a11y/anchor-is-valid": "off",
    "jsx-a11y/anchor-has-content": "off",
  },
};
