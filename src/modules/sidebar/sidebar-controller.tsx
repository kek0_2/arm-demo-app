import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { Sidebar } from "./sidebar";
import { getSidebarInfo } from "./sidebar-selectors";
import { fetchSidebarData } from "./sidebar-actions";

const mapStateToProps = createStructuredSelector({
  sidebarInfo: getSidebarInfo,
});

const mapDispatchToProps = {
  fetchSidebarData,
};

export const SidebarController = connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar);
