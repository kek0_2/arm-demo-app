import { Dispatch } from "redux";
import { ISidebarInfoParams } from "src/components/sidebar-info";
import { Util } from "src/utils/utils";
import { SERVER_URL, TABLES_NAME } from "src/config/server";

const SIDEBAR_NAMESPACE = "@sidebar";

export const SET_SIDEBAR_INFO = `${SIDEBAR_NAMESPACE}/SET_SIDEBAR_INFO`;
export const setSidebarInfo = (data: ISidebarInfoParams) => ({
  type: SET_SIDEBAR_INFO,
  payload: data,
});

export const fetchSidebarData = () => async (dispatch: Dispatch, getState) => {
  const state = getState();
  const strategy = state.headerParams.strategy;

  const fetchHeaderDataIrr = await Util.getFeaturesAPI(
    SERVER_URL,
    TABLES_NAME.HEADER_DATA_IRR,
    strategy
  );
  let dataIrr = fetchHeaderDataIrr.data[0];
  const fetchHeaderData = await Util.getFeaturesAPI(
    SERVER_URL,
    TABLES_NAME.HEADER_DATA,
    strategy
  );
  let data = fetchHeaderData.data[0];

  const arraySidebar = {
    area: dataIrr.area,
    ebitda: data.ebit_amount,
    ebitda_ga: data.ebit_amount_per_ha,
    nvp: dataIrr.npv,
    nvp_ga: dataIrr.npv_per_ha,
    irr: dataIrr.irr,
    ddp: dataIrr.ddp,
    investments: data.equip_cost,
    investments_ga: data.equip_cost_per_ha,
  };

  dispatch(setSidebarInfo(arraySidebar));
};
