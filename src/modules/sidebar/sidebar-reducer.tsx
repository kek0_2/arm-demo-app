import { SET_SIDEBAR_INFO } from "./sidebar-actions";
import { TSidebarState } from "./sidebar-type";

export const SIDEBAR_INITIAL_STATE: TSidebarState = {
  sidebarInfo: {
    area: 0,
    ebitda: 0,
    ebitda_ga: 0,
    nvp: 0,
    nvp_ga: 0,
    irr: 0,
    ddp: 0,
    investments: 0,
    investments_ga: 0,
  },
};

export const sidebarReducer = (state = SIDEBAR_INITIAL_STATE, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_SIDEBAR_INFO:
      return {
        ...state,
        sidebarInfo: payload,
      };
    default:
      return state;
  }
};
