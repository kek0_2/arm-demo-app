import { createSelector } from "reselect";
import { SIDEBAR_KEY } from "./sidebar-constants";
import { TSidebarState } from "./sidebar-type";

const getState = (appState): TSidebarState => appState[SIDEBAR_KEY];

export const getSidebarInfo = createSelector(
  getState,
  (state: TSidebarState) => state.sidebarInfo
);
