import React from "react";
import { SidebarTree } from "./sidebar-tree";
import { SidebarInfo } from "../../components/sidebar-info";
import "./sidebar.scss";

export const Sidebar = (props) => {
  const { sidebarInfo, fetchSidebarData } = props;

  React.useEffect(() => {
    fetchSidebarData();
  }, [fetchSidebarData]);

  return (
    <div className="sidebar">
      <div className="sidebar__tree">
        <SidebarTree />
      </div>
      <div className="sidebar__info">
        <SidebarInfo params={sidebarInfo} />
      </div>
    </div>
  );
};
