import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TreeView from "@material-ui/lab/TreeView";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import TreeItem from "@material-ui/lab/TreeItem";

const useStyles = makeStyles({
  root: {
    height: 216,
    flexGrow: 1,
    maxWidth: 400,
  },
});

export function SidebarTree() {
  const classes = useStyles();

  return (
    <TreeView
      className={classes.root}
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpandIcon={<ChevronRightIcon />}
      multiSelect
    >
      <TreeItem nodeId="1" label="Ключевые цели">
        <TreeItem nodeId="2" label="Основные показатели" />
        <TreeItem nodeId="3" label="Структура затрат" />
      </TreeItem>
      <TreeItem nodeId="5" label="Севооборот">
        <TreeItem nodeId="6" label="Карта" />
        <TreeItem nodeId="7" label="Графики" />
      </TreeItem>
      <TreeItem nodeId="5" label="Земля" />
      <TreeItem nodeId="5" label="Семена" />
      <TreeItem nodeId="5" label="Технологии" />
      <TreeItem nodeId="5" label="Инвестиции" />
    </TreeView>
  );
}
