import { ISidebarInfoParams } from "src/components/sidebar-info";

export type TSidebarState = {
  sidebarInfo: ISidebarInfoParams;
};
