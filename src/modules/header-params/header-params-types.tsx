export interface IHeaderParams {
  region: string;
  strategy: string;
  bp: string;
  intensification: boolean;
  deoxidation: boolean;
}

export interface IHeaderParamsProps {
  region: string;
  strategy: string;
  bp: string;
  intensification: boolean;
  deoxidation: boolean;
  setRegion: (region: string) => void;
  setStrategy: (strategy: string) => void;
  setIntensification: (intensification: boolean) => void;
  setDeoxidation: (deoxidation: boolean) => void;
}
