import React from "react";
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  FormGroup,
  FormControlLabel,
  Switch,
} from "@material-ui/core";
import { ORG_STRUCTURE } from "src/config/org-structure";
import { BUSINESS_PLAN_ENUM, STRATEGY_LIST } from "./header-params-constants";
import { IHeaderParamsProps } from "./header-params-types";
import "./header-params.scss";

export const HeaderParams = (props: IHeaderParamsProps) => {
  const { setRegion, setStrategy, setIntensification, setDeoxidation } = props;

  const onSelectedRegion = React.useCallback(
    e => {
      setRegion(e.target.value as string);
    },
    [setRegion]
  );

  const onSelectedStartegy = React.useCallback(
    e => {
      setStrategy(e.target.value as string);
    },
    [setStrategy]
  );

  const [bp, setBP] = React.useState<string>("бизнес план 2020");
  const onSelectedBp = (event: React.ChangeEvent<{ value: unknown }>) => {
    setBP(event.target.value as string);
  };

  const onChangeIntensification = React.useCallback(
    e => {
      let strategyCode;
      switch (props.strategy) {
        case "100":
          strategyCode = "101";
          break;
        case "101":
          strategyCode = "100";
          break;
        case "102":
          strategyCode = "103";
          break;
        case "103":
          strategyCode = "102";
          break;
      }
      setStrategy(strategyCode);
      setIntensification(e.target.checked as boolean);
    },
    [setIntensification, setStrategy, props.strategy]
  );

  const onChangeDeoxidation = React.useCallback(
    e => {
      setDeoxidation(e.target.checked as boolean);
    },
    [setDeoxidation]
  );

  return (
    <FormGroup row>
      <FormControl className="header-params__select">
        <InputLabel color="secondary">Регион</InputLabel>
        <Select
          value={props.region}
          onChange={onSelectedRegion}
          color="secondary"
        >
          {Object.keys(ORG_STRUCTURE).map(key => {
            const title = ORG_STRUCTURE[key].title;
            const code = ORG_STRUCTURE[key].code;
            return (
              <MenuItem value={title} key={code}>
                {title}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>

      <FormControl className="header-params__select">
        <InputLabel color="secondary">Стратегия</InputLabel>
        <Select
          value={props.strategy}
          onChange={onSelectedStartegy}
          color="secondary"
        >
          {STRATEGY_LIST.filter(
            obj => props.intensification === obj.intensification
          ).map(obj => {
            const title = obj.title;
            const code = obj.code;
            return (
              <MenuItem value={code} key={code}>
                {code + title}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>

      <FormControl className="header-params__select">
        <InputLabel color="secondary">Бизнем план</InputLabel>
        <Select value={bp} onChange={onSelectedBp} color="secondary">
          {Object.keys(BUSINESS_PLAN_ENUM).map(key => {
            const value = BUSINESS_PLAN_ENUM[key];
            return (
              <MenuItem value={value} key={key}>
                {value}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>

      <FormControlLabel
        control={
          <Switch
            checked={props.intensification}
            onChange={onChangeIntensification}
            value={props.intensification}
            size="small"
          />
        }
        label="интенсификация"
        className="header-params__switch"
      />
      <FormControlLabel
        control={
          <Switch
            checked={props.deoxidation}
            onChange={onChangeDeoxidation}
            value={props.deoxidation}
            size="small"
          />
        }
        label="раскисление"
        className="header-params__switch"
      />
    </FormGroup>
  );
};
