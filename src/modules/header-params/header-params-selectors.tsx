import { createSelector } from "reselect";
import { HEADER_PARAMS_KEY } from "./header-params-constants";
import { IHeaderParams } from "./header-params-types";

const getState = (appState): IHeaderParams => appState[HEADER_PARAMS_KEY];

export const getRegion = createSelector(
  getState,
  (state: IHeaderParams) => state.region
);

export const getStrategy = createSelector(
  getState,
  (state: IHeaderParams) => state.strategy
);

export const getBp = createSelector(
  getState,
  (state: IHeaderParams) => state.bp
);

export const getIntensification = createSelector(
  getState,
  (state: IHeaderParams) => state.intensification
);

export const getDeoxidation = createSelector(
  getState,
  (state: IHeaderParams) => state.deoxidation
);
