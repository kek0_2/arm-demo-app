export const HEADER_PARAMS_KEY = "headerParams";

export enum REGIONS_ENUM {
  OREL = "Орел",
  BELGOROD = "Белгород",
  KURSK = "Белгород-Курск",
  TAMBOV = "Тамбов",
}

export const STRATEGY_LIST = [
  {
    title: "С потребностью в сах. свекле",
    code: "100",
    sugar_beer: true,
    intensification: false,
  },
  {
    title: "С потребностью в сах. свекле",
    code: "101",
    sugar_beer: true,
    intensification: true,
  },
  {
    title: "Без потребности в сах. свекле",
    code: "102",
    sugar_beer: false,
    intensification: false,
  },
  {
    title: "Без потребности в сах. свекле",
    code: "103",
    sugar_beer: false,
    intensification: true,
  },
];

export enum BUSINESS_PLAN_ENUM {
  BUSINESS_PLAN_2020 = "бизнес план 2020",
  BUSINESS_PLAN_2021 = "бизнес план 2021",
  BUSINESS_PLAN_2022 = "бизнес план 2022",
}
