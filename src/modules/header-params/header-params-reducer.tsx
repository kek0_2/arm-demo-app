import { IHeaderParams } from "./header-params-types";
import {
  SET_REGION,
  SET_STRATEGY,
  SET_INTENSEFICATION,
  SET_DEOXIDATION,
} from "./header-params-actions";

export const HEADER_PARAMS_INITIAL_STATE: IHeaderParams = {
  region: "Орел",
  strategy: "101",
  bp: "бизнес план 2020",
  intensification: true,
  deoxidation: false,
};

export const headerParamsReducer = (
  state = HEADER_PARAMS_INITIAL_STATE,
  action
) => {
  const { type, payload } = action;
  switch (type) {
    case SET_REGION:
      console.info(payload);
      return {
        ...state,
        region: payload,
      };
    case SET_STRATEGY:
      return {
        ...state,
        strategy: payload,
      };
    case SET_INTENSEFICATION:
      return {
        ...state,
        intensification: payload,
      };
    case SET_DEOXIDATION:
      return {
        ...state,
        deoxidation: payload,
      };
    default:
      return state;
  }
};
