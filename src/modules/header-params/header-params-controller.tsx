import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { HeaderParams } from "./header-params";
import {
  getRegion,
  getStrategy,
  getBp,
  getIntensification,
  getDeoxidation,
} from "./header-params-selectors";
import {
  setRegion,
  setStrategy,
  setIntensification,
  setDeoxidation,
} from "./header-params-actions";

const mapStateToProps = createStructuredSelector({
  region: getRegion,
  strategy: getStrategy,
  bp: getBp,
  intensification: getIntensification,
  deoxidation: getDeoxidation,
});

const mapDispatchToProps = {
  setRegion,
  setStrategy,
  setIntensification,
  setDeoxidation,
};

export const HeaderParamsController = connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderParams);
