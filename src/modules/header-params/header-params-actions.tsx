import { batch } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { fetchSidebarData } from "../sidebar/sidebar-actions";

const HEADER_PARAMS_NAMESPACE = "@headerParams";

export const SET_REGION = `${HEADER_PARAMS_NAMESPACE}/SET_REGION`;
export const setRegion = (region: string) => ({
  type: SET_REGION,
  payload: region,
});

export const SET_STRATEGY = `${HEADER_PARAMS_NAMESPACE}/SET_STRATEGY`;
export const setStrategy = (strategy: string) => async (
  dispatch: ThunkDispatch<{}, {}, any>
) => {
  batch(() => {
    dispatch({
      type: SET_STRATEGY,
      payload: strategy,
    });

    dispatch(fetchSidebarData());
  });
};

export const SET_INTENSEFICATION = `${HEADER_PARAMS_NAMESPACE}/SET_INTENSEFICATION`;
export const setIntensification = (param: boolean) => ({
  type: SET_INTENSEFICATION,
  payload: param,
});

export const SET_DEOXIDATION = `${HEADER_PARAMS_NAMESPACE}/SET_DEOXIDATION`;
export const setDeoxidation = (param: boolean) => ({
  type: SET_DEOXIDATION,
  payload: param,
});
