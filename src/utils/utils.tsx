import {
  VERSION_S,
  VARNT_BP,
  VERSION_BP,
  BUSINESS_AREA_TEXT,
} from "src/config/server";

/**
 * Query features from server
 * @param {string} serverUrl base url to server with workspace
 * @param {string} tableName table's name where to query
 * @param {string} startegy number of strategy to query
 * @param {string=} crop Optional. Name of crop to query test
 */
const getFeaturesAPI = async (
  serverUrl: string,
  tableName: string,
  startegy: string,
  crop?: string
) => {
  const url = new URL(`${serverUrl}/${tableName}`);
  const searchParams = new URLSearchParams();
  searchParams.append("varnt_s", startegy);
  searchParams.append("business_area_text", BUSINESS_AREA_TEXT);
  searchParams.append("version_s", VERSION_S);
  searchParams.append("varnt_bp", VARNT_BP);
  searchParams.append("version_bp", VERSION_BP);
  searchParams.append("plant", "5701,5702");
  if (crop) {
    searchParams.append("crop_id", crop);
  }
  try {
    url.search = searchParams.toString();
    const response = await fetch(url.toString(), {});
    const jsonResponse = await response.json();
    return jsonResponse;
  } catch {
    console.log(`Ошибка загрузки данных.`);
    return;
  }
};

export class Util {
  static getFeaturesAPI = getFeaturesAPI;
}
