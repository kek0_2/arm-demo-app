import React from "react";
import { ANALYTICS_PARAMS } from "./sidebar-info-constants";
import { ISidebarInfoProps, ISidebarInfoParams } from "./sidebar-info-type";
import { NumberFormatter } from "src/utils/formatters";
import "./sidebar-info.scss";

const renderList = (params: ISidebarInfoParams) => {
  return Object.keys(params).map(key => {
    let title = ANALYTICS_PARAMS[key].text;
    let units = ANALYTICS_PARAMS[key].units;
    let value = NumberFormatter.format(params[key]);
    return (
      <div className="sidebar-info__item" key={key}>
        <div className="sidebar-info__item__title">
          {title}
          <span>{units}</span>
        </div>
        <div className="sidebar-info__item__value">{value}</div>
      </div>
    );
  });
};

export const SidebarInfo = (props: ISidebarInfoProps) => {
  return <div className="sidebar-info">{renderList(props.params)}</div>;
};
