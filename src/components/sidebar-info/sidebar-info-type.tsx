export interface ISidebarInfoProps {
  params: ISidebarInfoParams;
}

export interface ISidebarInfoParams {
  area: number;
  ebitda: number;
  ebitda_ga: number;
  nvp: number;
  nvp_ga: number;
  irr: number;
  ddp: number;
  investments: number;
  investments_ga: number;
}
