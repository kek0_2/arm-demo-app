export enum ANALYTICS_PARAMS_ENUM {
  AREA = "area",
  EBITDA = "ebitda",
  EBITDA_GA = "ebitda_ga",
  NVP = "nvp",
  NVP_GA = "nvp_ga",
  IRR = "irr",
  DDP = "ddp",
  INVESTMENTS = "investments",
  INVESTMENTS_GA = "investments_ga",
}

export const ANALYTICS_PARAMS = {
  [ANALYTICS_PARAMS_ENUM.AREA]: {
    value: ANALYTICS_PARAMS_ENUM.AREA,
    text: "площадь",
    units: "Га",
  },
  [ANALYTICS_PARAMS_ENUM.EBITDA]: {
    value: ANALYTICS_PARAMS_ENUM.EBITDA,
    text: "ebitda",
    units: "млн. ₽",
  },
  [ANALYTICS_PARAMS_ENUM.EBITDA_GA]: {
    value: ANALYTICS_PARAMS_ENUM.EBITDA_GA,
    text: "ebitda на Га",
    units: "₽/Га",
  },
  [ANALYTICS_PARAMS_ENUM.NVP]: {
    value: ANALYTICS_PARAMS_ENUM.NVP,
    text: "NVP",
    units: "млн. ₽",
  },
  [ANALYTICS_PARAMS_ENUM.NVP_GA]: {
    value: ANALYTICS_PARAMS_ENUM.NVP_GA,
    text: "NVP на Га",
    units: "₽/Га",
  },
  [ANALYTICS_PARAMS_ENUM.IRR]: {
    value: ANALYTICS_PARAMS_ENUM.IRR,
    text: "IRR",
    units: "%",
  },
  [ANALYTICS_PARAMS_ENUM.DDP]: {
    value: ANALYTICS_PARAMS_ENUM.DDP,
    text: "DDP",
    units: "млн. ₽",
  },
  [ANALYTICS_PARAMS_ENUM.INVESTMENTS]: {
    value: ANALYTICS_PARAMS_ENUM.INVESTMENTS,
    text: "инвестиции",
    units: "млрд. ₽",
  },
  [ANALYTICS_PARAMS_ENUM.INVESTMENTS_GA]: {
    value: ANALYTICS_PARAMS_ENUM.INVESTMENTS_GA,
    text: "инвестиции на Га",
    units: "₽/Га",
  },
};
