import React from "react";
import { AppBar, Toolbar, Typography, FormGroup } from "@material-ui/core";
import { IHeaderType } from "./header-types";
import "./header.scss";

export const Header = (props: IHeaderType) => {
  return (
    <AppBar position="static" className="header">
      <FormGroup row>
        <Toolbar>
          <Typography
            style={{ width: "200px", fontWeight: "bolder", fontSize: "24px" }}
          >
            РУСАГРО
          </Typography>
        </Toolbar>
        {props.children}
        <Toolbar>
          <div className="header__employee-text">ОТВЕТСТВЕННЫЙ ЗА РЕГИОН</div>
          <div className="header__employee"> Лисицкий А.М.</div>
        </Toolbar>
      </FormGroup>
    </AppBar>
  );
};
