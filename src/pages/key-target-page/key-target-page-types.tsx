export interface IKeyTargetPageProps {
  selectedCrop: string;
  selectedPO: string[];
  setSelectedCrop: (crop: string) => void;
}
