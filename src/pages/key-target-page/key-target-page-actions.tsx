import { Dispatch } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { batch } from "react-redux";
import { KEY_TARGET_PAGE_STATE_KEY } from "./key-target-page-constants";

const KEY_TARGET_PAGE_NAMESPACE = "@keyTargetPage";

export const SET_SELECTED_CROP = `${KEY_TARGET_PAGE_NAMESPACE}/SET_SELECTED_CROP`;
export const setSelectedCrop = (crop: string) => async (
  dispatch: ThunkDispatch<{}, {}, any>
) => {
  batch(() => {
    dispatch({
      type: SET_SELECTED_CROP,
      payload: crop,
    });

    dispatch(fetchInfoData());
  });
};

export const SET_INFO_CROP_DATA = `${KEY_TARGET_PAGE_NAMESPACE}/SET_INFO_CROP_DATA`;
export const setInfoCropData = (data: any) => ({
  type: SET_INFO_CROP_DATA,
  payload: data,
});

export const fetchInfoData = () => async (dispatch: Dispatch, getState) => {
  // debugger;
  const state = getState()[KEY_TARGET_PAGE_STATE_KEY];
  console.info(state);

  const arrayInfo = {
    ebitda: { value: 4280.77, perc: 191 },
    ebitda_ga: { value: 42678.34, perc: 208 },
    area: { value: 42678.34, perc: -15 },
    productivity: { value: 386.7, perc: 1.49 },
    costs_ga: { value: 79334.6, perc: -9.44 },
    price: { value: 2.345, perc: 46.5 },
  };

  // const featureCollection = await Util.queryFeatures();

  dispatch(setInfoCropData(arrayInfo));
};
