export { KEY_TARGET_PAGE_STATE_KEY } from "./key-target-page-constants";
export { keyTargetPageReducer } from "./key-target-page-reducer";
export { KeyTargetPageController as KeyTargetPage } from "./key-target-page-controller";
