import { createSelector } from "reselect";
import { KEY_TARGET_PAGE_STATE_KEY } from "./key-target-page-constants";

const getState = appState => {
  return appState[KEY_TARGET_PAGE_STATE_KEY];
};

export const getSelectedCrop = createSelector(
  getState,
  state => state.selectedCrop
);

export const getInfoCropData = createSelector(
  getState,
  state => state.infoCropData
);
