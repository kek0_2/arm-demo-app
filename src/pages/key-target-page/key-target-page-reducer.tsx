import {
  SET_SELECTED_CROP,
  SET_INFO_CROP_DATA,
} from "./key-target-page-actions";

export const KEY_TARGET_PAGE_INITIAL_STATE = {
  selectedCrop: "barley",
  selectedPO: [],
  infoCropData: {
    ebitda: { value: 0, perc: 0 },
    ebitda_ga: { value: 0, perc: 0 },
    area: { value: 0, perc: 0 },
    productivity: { value: 0, perc: 0 },
    costs_ga: { value: 0, perc: 0 },
    price: { value: 0, perc: 0 },
  },
};

export const keyTargetPageReducer = (
  state = KEY_TARGET_PAGE_INITIAL_STATE,
  action
) => {
  const { type, payload } = action;
  switch (type) {
    case SET_SELECTED_CROP:
      return {
        ...state,
        selectedCrop: payload,
      };
    case SET_INFO_CROP_DATA:
      return {
        ...state,
        infoCropData: payload,
      };
    default:
      return state;
  }
};
