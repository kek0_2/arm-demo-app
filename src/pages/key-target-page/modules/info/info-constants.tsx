export enum INFO_COMPONENT_ENUM {
  EBITDA = "ebitda",
  EBITDA_GA = "ebitda_ga",
  AREA = "area",
  PRODUCTIVITY = "productivity",
  COSTS_GA = "costs_ga",
  PRICE = "price",
}

export const INFO_COMPONENT_PARAMS = {
  [INFO_COMPONENT_ENUM.EBITDA]: {
    value: INFO_COMPONENT_ENUM.EBITDA,
    text: "ebitda",
    units: "млн. ₽",
  },
  [INFO_COMPONENT_ENUM.EBITDA_GA]: {
    value: INFO_COMPONENT_ENUM.EBITDA_GA,
    text: "ebitda на Га",
    units: "₽/Га",
  },
  [INFO_COMPONENT_ENUM.AREA]: {
    value: INFO_COMPONENT_ENUM.AREA,
    text: "площадь",
    units: "Га",
  },
  [INFO_COMPONENT_ENUM.PRODUCTIVITY]: {
    value: INFO_COMPONENT_ENUM.PRODUCTIVITY,
    text: "урожайность",
    units: "ц/Га",
  },
  [INFO_COMPONENT_ENUM.COSTS_GA]: {
    value: INFO_COMPONENT_ENUM.COSTS_GA,
    text: "затраты на Га",
    units: "₽/Га",
  },
  [INFO_COMPONENT_ENUM.PRICE]: {
    value: INFO_COMPONENT_ENUM.PRICE,
    text: "цена",
    units: "₽",
  },
};
