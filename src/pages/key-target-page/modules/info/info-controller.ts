import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import {
  getSelectedCrop,
  getInfoCropData,
} from "src/pages/key-target-page/key-target-page-selectors";
import { InfoComponent } from "./info";

const mapStateToProps = createStructuredSelector({
  selectedCrop: getSelectedCrop,
  infoCropData: getInfoCropData,
});

export const InfoComponentController = connect(mapStateToProps)(InfoComponent);
