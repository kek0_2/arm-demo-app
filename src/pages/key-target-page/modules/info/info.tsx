import React from "react";
import { NumberFormatter } from "src/utils/formatters";
import { INFO_COMPONENT_PARAMS } from "./info-constants";
import { CROPS } from "src/config/crops";
import "./info.scss";

export const InfoComponent = props => {
  const { selectedCrop, infoCropData } = props;
  return (
    <div className="info">
      <div className="info__title">{CROPS[selectedCrop].alias}</div>
      {Object.keys(infoCropData).map(key => {
        let alias = INFO_COMPONENT_PARAMS[key].text;
        let units = INFO_COMPONENT_PARAMS[key].units;
        let value = NumberFormatter.format(infoCropData[key].value);
        let percent = NumberFormatter.format(infoCropData[key].perc);
        let dimanics = infoCropData[key].perc < 0 ? "negative" : "positive";

        return (
          <div className="info__item" key={key}>
            <div className="info-item-label">
              {alias}
              <span>{units}</span>
            </div>
            <div className="info-item-value">{value}</div>
            <div className={`info-item-share ${dimanics}`}>
              {percent}
              <div className={`info-item-share-change ${dimanics}`}>
                {dimanics === "negative" && <span>{"%"}</span>}
                <svg
                  className="arrow"
                  focusable="false"
                  viewBox="2 3 15 15"
                  aria-hidden="true"
                  role="presentation"
                >
                  <path d="M4 7l6 6 6-6z"></path>
                </svg>
                {dimanics === "positive" && <span>{"%"}</span>}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};
