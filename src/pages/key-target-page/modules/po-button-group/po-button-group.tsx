import React from "react";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import "./po-button-group.scss";

const listPo1 = {
  po0: { name: "Все ПО", disabled: false },
  po1: { name: "отА ПО №1 Мценск", disabled: false },
  po2: { name: "отА ПО №1 Нечаево", disabled: true },
};

export const POButtonGroup = props => {
  return (
    <div className="po-button-group">
      <div className="po-button-group__title">
        КЛЮЧЕВЫЕ ЦЕЛИ
        <span>ПО</span>
      </div>

      <ButtonGroup color="primary" aria-label="outlined primary button group">
        {Object.keys(listPo1).map(key => (
          <Button
            key={key}
            className={listPo1[key].disabled ? "disactive" : ""}
          >
            {listPo1[key].name}
          </Button>
        ))}
      </ButtonGroup>
    </div>
  );
};
