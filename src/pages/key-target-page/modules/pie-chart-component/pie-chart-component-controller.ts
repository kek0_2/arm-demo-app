import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { setSelectedCrop } from "../../key-target-page-actions";
import { PieChartComponent } from "./pie-chart-component";
import { getSelectedCrop } from "src/pages/key-target-page/key-target-page-selectors";

const mapStateToProps = createStructuredSelector({
  selectedCrop: getSelectedCrop,
});

const mapDispatchToProps = {
  setSelectedCrop,
};

export const PieChartComponentController = connect(
  mapStateToProps,
  mapDispatchToProps
)(PieChartComponent);
