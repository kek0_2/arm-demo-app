import React from "react";
import { PieChart, Pie, Sector, Cell, Label } from "recharts";
import { CROPS } from "src/config/crops";

const data = [
  { name: "soybeans", value: 400, value1: 29 },
  { name: "sugar_beer", value: 300, value1: 84 },
  { name: "winter_weat", value: 300, value1: 83 },
  { name: "barley", value: 200, value1: 55 },
  { name: "sidereal_pairs", value: 200, value1: 46 },
  { name: "sunflowers", value: 200, value1: 15 },
  { name: "spring_weat", value: 200, value1: 23 },
];

let dataNow = data.map(key => {
  let index = CROPS[key.name].index;
  return { name: key.name, value: key.value, value1: key.value1, index: index };
});
dataNow.sort((a, b) => a.index - b.index);

const renderActiveShape = props => {
  const {
    cx,
    cy,
    innerRadius,
    outerRadius,
    startAngle,
    endAngle,
    fill,
  } = props;

  return (
    <g>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={30}
        outerRadius={outerRadius + 10}
        fill={fill}
        fillOpacity={0.4}
      />
    </g>
  );
};

const renderCustomizedLabel2 = props => {
  const RADIAN = Math.PI / 180;
  const { cx, cy, midAngle, outerRadius, fill, percent, value } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 0) * cos;
  const sy = cy + (outerRadius + 0) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 22;
  const ey = my;
  const textAnchor = cos >= 0 ? "start" : "end";

  return (
    <g>
      <path
        d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
        stroke={fill}
        fill="none"
      />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 12}
        y={ey}
        textAnchor={textAnchor}
        fill="#dadada"
        fontSize="11px"
      >{`${value}`}</text>
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 12}
        y={ey}
        dy={12}
        textAnchor={textAnchor}
        fill="#929292"
        fontSize="9px"
      >
        {`${(percent * 100).toFixed(2)}%`}
      </text>
    </g>
  );
};

export const PieChartComponent = props => {
  const { selectedCrop, setSelectedCrop } = props;

  let activeIndex = CROPS[selectedCrop].index;

  const onSelectedCropChanged = React.useCallback(
    data => {
      setSelectedCrop(data.name as string);
    },
    [setSelectedCrop]
  );

  return (
    <PieChart width={600} height={600}>
      <text x={300} y={20} fill={"#dadada"} textAnchor="middle">
        Бизнес план 2020 - 2021
      </text>
      <Pie
        activeIndex={activeIndex}
        activeShape={renderActiveShape}
        data={dataNow}
        cx={300}
        cy={140}
        innerRadius={60}
        outerRadius={80}
        startAngle={90}
        endAngle={-270}
        fill="#dadada"
        dataKey="value"
        onClick={onSelectedCropChanged}
        label={renderCustomizedLabel2}
      >
        {dataNow.map((entry, index) => (
          <Cell
            key={`cell-${index}`}
            fill={CROPS[entry.name].color}
            strokeWidth={0}
          />
        ))}
        <Label value={12345} position="center" fill={"#dadada"} />
      </Pie>
      <text x={300} y={300} fill={"#dadada"} textAnchor="middle">
        Бизнес план 2022 - 2026
      </text>
      <Pie
        activeIndex={activeIndex}
        activeShape={renderActiveShape}
        data={dataNow}
        cx={300}
        cy={420}
        innerRadius={60}
        outerRadius={80}
        startAngle={90}
        endAngle={-270}
        fill="#dadada"
        dataKey="value1"
        onClick={onSelectedCropChanged}
        label={renderCustomizedLabel2}
      >
        {dataNow.map((entry, index) => (
          <Cell
            key={`cell-${index}`}
            fill={CROPS[entry.name].color}
            strokeWidth={0}
          />
        ))}
        <Label value={12345} position="center" fill={"#dadada"} />
      </Pie>
    </PieChart>
  );
};
