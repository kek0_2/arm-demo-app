import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { setSelectedCrop } from "../../key-target-page-actions";
import { Legend } from "./legend";
import { getSelectedCrop } from "./legend-selectors";

const mapStateToProps = createStructuredSelector({
  selectedCrop: getSelectedCrop,
});

const mapDispatchToProps = {
  setSelectedCrop,
};

export const LegendController = connect(
  mapStateToProps,
  mapDispatchToProps
)(Legend);
