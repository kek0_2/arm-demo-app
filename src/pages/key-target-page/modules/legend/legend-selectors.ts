import { createSelector } from "reselect";
import { KEY_TARGET_PAGE_STATE_KEY } from "src/pages/key-target-page";

const getState = appState => {
  return appState[KEY_TARGET_PAGE_STATE_KEY];
};

export const getSelectedCrop = createSelector(
  getState,
  state => state.selectedCrop
);
