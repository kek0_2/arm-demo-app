import React from "react";
import { CROPS } from "src/config/crops";
import "./legend.scss";

export const Legend = props => {
  const { selectedCrop, setSelectedCrop } = props;

  const onSelectedCropChanged = React.useCallback(
    crop => {
      setSelectedCrop(crop as string);
    },
    [setSelectedCrop]
  );

  return (
    <div className="legend">
      {Object.keys(CROPS).map(crop => {
        return (
          <div
            className={
              selectedCrop === crop
                ? `legend__item ${crop} active`
                : `legend__item ${crop}`
            }
            key={`legend_${crop}`}
            onClick={() => onSelectedCropChanged(crop)}
          >
            <div className={`legend__crop ${crop}`}>
              <div className="arrow-down"></div>
            </div>
            <div className="legend__crop__label">{CROPS[crop].alias}</div>
          </div>
        );
      })}
    </div>
  );
};
