import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { setSelectedCrop } from "./key-target-page-actions";
import { getSelectedCrop } from "./key-target-page-selectors";
import { KeyTargetPage } from "./key-target-page";

const mapStateToProps = createStructuredSelector({
  selectedCrop: getSelectedCrop,
});

const mapDispatchToProps = {
  setSelectedCrop,
};

export const KeyTargetPageController = connect(
  mapStateToProps,
  mapDispatchToProps
)(KeyTargetPage);
