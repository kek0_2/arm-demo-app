import React from "react";
import { IKeyTargetPageProps } from "./key-target-page-types";
import { Header } from "../../components/header";
import { Sidebar } from "../../modules/sidebar";
import { HeaderParams } from "../../modules/header-params";
import { POButtonGroup } from "./modules/po-button-group";
import { InfoComponent } from "./modules/info";
import { PieChartComponent } from "./modules/pie-chart-component";
import { Legend } from "./modules/legend";
import "./key-target-page.scss";

export function KeyTargetPage(props: IKeyTargetPageProps) {
  return (
    <div className="key-target-page">
      <Header>
        <HeaderParams />
      </Header>
      <div className="key-target-page__sidebar">
        <Sidebar />

        <div className="key-target-page__content">
          <POButtonGroup />
          <div className="page-panel">
            <div className="page-panel__title">EBITDA</div>
            <div className="page-panel__content">
              <Legend />
              <PieChartComponent />
              <InfoComponent />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
