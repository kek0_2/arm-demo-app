import { combineReducers } from "redux";
import {
  HEADER_PARAMS_KEY,
  headerParamsReducer,
} from "./modules/header-params";
import { SIDEBAR_KEY, sidebarReducer } from "./modules/sidebar";
import {
  KEY_TARGET_PAGE_STATE_KEY,
  keyTargetPageReducer,
} from "./pages/key-target-page";

const dynamicReducers = {
  [HEADER_PARAMS_KEY]: headerParamsReducer,
  [SIDEBAR_KEY]: sidebarReducer,
  [KEY_TARGET_PAGE_STATE_KEY]: keyTargetPageReducer,
};

export default combineReducers(dynamicReducers);
