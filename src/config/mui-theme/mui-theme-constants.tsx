export const PRIMARY_THEME_COLOR = "#484a4d";
export const PRIMARY_TEXT_COLOR = "#dadada";

export const SECONDARY_THEME_COLOR = "#a2c573";
