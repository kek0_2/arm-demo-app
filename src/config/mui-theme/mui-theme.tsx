import { createMuiTheme } from "@material-ui/core/styles";
import {
  PRIMARY_THEME_COLOR,
  PRIMARY_TEXT_COLOR,
  SECONDARY_THEME_COLOR,
} from "./mui-theme-constants";

export const MuiTheme = createMuiTheme({
  palette: {
    primary: {
      main: PRIMARY_THEME_COLOR,
      contrastText: PRIMARY_TEXT_COLOR,
    },
    secondary: {
      main: SECONDARY_THEME_COLOR,
      contrastText: PRIMARY_TEXT_COLOR,
    },
  },
});
