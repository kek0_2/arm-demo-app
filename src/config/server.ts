export const SERVER_HOST = "http://gitlab.rainvest.ru:3000";
export const SERVER_WORKSPACE = "arm_gd";
export const SERVER_URL = `${SERVER_HOST}/${SERVER_WORKSPACE}`;

export const TABLES_NAME = {
  HEADER_DATA: "head_data", // данные для sidebar панели приложения
  HEADER_DATA_IRR: "get_head_data_irr", // данные для sidebar панели приложения
  CROP_INFO_DATA: "get_difference_between_values", // данные для инфоокна по культуре
};

export const VERSION_S = "19902"; // версия стратегии
export const VARNT_BP = "100"; // вариант плана
export const VERSION_BP = "20001"; // версия плана
export const BUSINESS_AREA_TEXT = "Орел"; // регион
