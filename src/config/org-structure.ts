export const ORG_STRUCTURE = {
  belgorod: {
    title: "Белгород",
    code: "3100",
    plants: [
      { code: "3107", title: "ПО Белоколодезянское" },
      { code: "3104", title: "ПО Закутское" },
      { code: "3110", title: "ПО Заречье" },
      { code: "3106", title: "ПО Казинское" },
      { code: "3108", title: "ПО Ютановское" },
      { code: "3131", title: "КША Агроинновационный центр" },
    ],
  },
  belgorodKursk: {
    title: "Белгород-Курск",
    code: "4600",
    plants: [
      { code: "3102", title: "ПО Истобнянское" },
      { code: "4601", title: "ПО Кшень" },
      { code: "3101", title: "ПО Чернянское" },
    ],
  },
  orel: {
    title: "Орел",
    code: "5700",
    plants: [
      { code: "5701", title: "ПО Мценск" },
      { code: "5702", title: "ПО Нечаево" },
    ],
  },
  tambov: {
    title: "Тамбов",
    code: "6800",
    plants: [
      { code: "6803", title: "ПО Дмитриевское" },
      { code: "6801", title: "ПО Жердевское" },
      { code: "6806", title: "ПО Знаменское" },
      { code: "6804", title: "ПО Тамбовское" },
    ],
  },
};
