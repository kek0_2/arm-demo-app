export const CROPS = {
  barley: {
    index: 0,
    id: "0025",
    value: "barley",
    alias: "ячмень",
    color: "#4888f1",
  },
  sidereal_pairs: {
    index: 1,
    id: "0010",
    value: "sidereal_pairs",
    alias: "сидеральные пары",
    color: "#cb7d24",
  },
  winter_weat: {
    index: 2,
    id: "0012",
    value: "winter_weat",
    alias: "озимая пшеница",
    color: "#9bb848",
  },
  spring_weat: {
    index: 3,
    id: "0013",
    value: "spring_weat",
    alias: "яровая пшеница",
    color: "#88bca7",
  },
  sugar_beer: {
    index: 4,
    id: "0017",
    value: "sugar_beer",
    alias: "сахарная свекла",
    color: "#ff9a9a",
  },
  soybeans: {
    index: 5,
    id: "0022",
    value: "soybeans",
    alias: "соя",
    color: "#93b6eb",
  },
  sunflowers: {
    index: 6,
    id: "0011",
    value: "sunflowers",
    alias: "подсолнечник",
    color: "#fedf3f",
  },
};
