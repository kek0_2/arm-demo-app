import React from "react";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import { KeyTargetPage } from "./pages/key-target-page";
import { PATHS } from "./constant";

export const AppRouter = () => {
  return (
    <Router>
      <Switch>
        <Route exact path={PATHS.KEY_TARGET}>
          <KeyTargetPage />
        </Route>
        <Route path={PATHS.CROP_ROTATION}>
          <About />
        </Route>
        <Route path={PATHS.LAND}>
          <About />
        </Route>
      </Switch>
    </Router>
  );
};

function About() {
  return (
    <div>
      <h2>About</h2>
    </div>
  );
}
